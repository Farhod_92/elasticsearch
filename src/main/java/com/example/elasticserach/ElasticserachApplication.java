package com.example.elasticserach;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ElasticserachApplication {

    public static void main(String[] args) {
        SpringApplication.run(ElasticserachApplication.class, args);
    }

}
